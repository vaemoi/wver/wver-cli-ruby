require 'fileutils'
require 'json'
require 'logger'
require 'net/http'
require 'pstore'
require 'uri'

# CLI upload tool for rev. revit:
#       0) Authenticates you using Auth0
#       1) Takes a combination of inputs
#         * File path
#         * Directory path
#         * git/svn repo path
#           - git/svn repo commit hashes
#           - git/svn repo branch names
#             ~ git svn repo branch commit hashes
#       2) Creates a handy dandy rev
#       3) Notifies your melee
#       3) Gives you a web link to the rev
#
# @author [brwnrclse]
#
module Revit
  # Setup home folder for config and log(s)
  FileUtils.mkdir_p("#{Dir.home}/.revit")
  STORE = PStore.new("#{Dir.home}/.revit/revit.pstore")
  LOGGER = Logger.new("#{Dir.home}/.revit/revit.log")

  # Set default values for store values if there aren't any
  STORE.transaction do |store|
    store[:auth0_id] = nil unless store[:auth0_id]
    store[:access_token] = nil unless store[:access_token]
    store[:auth_url] = 'https://vaemoi.auth0.com'.freeze unless store[:auth_url]
    store[:user_id] = nil unless store[:user_id]

    unless store[:auth_callback_url]
      store[:auth_callback_url] = 'https://rev.vaemoi.co/login_success'.freeze
    end

    unless store[:auth_client_id]
      store[:auth_client_id] = 'Q1fRDQ9u3oN33ok0ciIi9Vww5kV8U8MA'.freeze
    end

    unless store[:schema_path]
      store[:schema_path] = "#{Dir.pwd}/lib/revit/schema/schema.json".freeze
    end

    unless store[:endpoint_url]
      store[:endpoint_url] = 'https://api.graph.cool/simple/v1/revwver'.freeze
    end

    # Caching the graphql schema
    unless File.exist?(store[:schema_path]) && !File.zero?(store[:schema_path])
      uri = URI.parse(store[:endpoint_url])
      req = Net::HTTP::Post.new(uri)
      req.content_type = 'application/json'
      req.body = { query: '{__schema {types {name}}}' }.to_json
      http = Net::HTTP.new(uri.host, uri.port)
      File.write(store[:schema_path], JSON.parse(http.request(req).body))
    end
  end

  LOGGER.datetime_format = '%<year>Y-%<month>m-%<day>d %<hour>H:%<min>M:%<s>S '
end

require_relative 'revit/api'
require_relative 'revit/auth'
require_relative 'revit/cli'
require_relative 'revit/exception'
require_relative 'revit/util'
require_relative 'revit/version'
