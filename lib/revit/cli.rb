require 'paint'
require 'thor'
require 'tty-spinner'

# rubocop: disable Metrics/AbcSize, Metrics/MethodLength

module Revit
  #
  # Rev's cli interface
  #
  # @author [brwnrclse]
  #
  class CLI < Thor
    namespace :revit

    map %w(--version -v) => :__print_version
    desc '--version, -v', 'print the version'
    def __print_version
      puts VERSION
    end

    method_option :description, type: :string, aliases: '-d', lazy_default: '',
                                desc: 'A short summary of the changes'
    method_option :list_files, type: :array, aliases: '-l',
                               desc: 'A list of files to add to the rev'
    method_option :title, type: :string, aliases: '-t', lazy_default: '',
                          desc: 'A title for the rev'
    method_option :no_vc, type: :boolean, aliases: '-i', lazy_default: false,
                          desc: 'Ignore vesion control when parsing'
    desc 'create PATH {parmas...}', 'Makes a new rev from the PATH'
    long_desc <<-LONGDESC
      `Initiates a flow for creating a new rev. To bypass the prompts users can use the provided flags.
      If using git the title, description and files will be retrieved from git via the git repo name, latest commit msg and latest commit file list respectively.

      `revit create ./Bucket/repo` # Create a new rev from the provided path
      `revit create ./Bucket/repo {--title, -t} "Test Repo"` # Create a new rev with a title of Test Repo
      `revit create ./Bucket/repo {--description, -d} "A test repo."` # Create a new rev with a description of "a test repo"
      `revit create ./Bucket/repo {--list_files, -l} ~/Bucket/oneoffs/help.js` # Create a new rev with a list of files, ignoring the current folder
      `revit create ./Bucket/repo {--ignore_vc, -i} # Create a new rev from a repo using only the files not commits+diffs
    LONGDESC
    def create(path)
      rev_id = nil
      paths = path.split
      paths ||= options[:list_files] if options[:list_files]
      spinner = TTY::Spinner.new('[:spinner] :msg', format: :bouncing_ball)

      spinner.update(msg: 'Creating a rev...')
      spinner.run do
        rev_id = RevAPI.create_rev(options[:title], options[:description])
      end
      spinner.success(Paint['rev created', :green])

      docs = Util.collect_docs(paths, spinner)
      successful_docs = []

      spinner.update(msg: 'Uploading docs to rev api')

      docs.each do |doc|
        spinner.run do
          doc_id = RevAPI.create_doc(doc.doc_name, doc.has_diff, rev_id,
                                     doc.syntax)
          # TODO: Add in Auth.signFile call
          # TODO: Add in RevAPI.create_s3Resource call
          spinner.success(Paint['doc uploaded to api', :green])

          # Collect successful doc_ids incase of failure
          successful_docs.insert(doc_id)
        end
      end

      puts "\nrev available at #{Paint["wver.vaemoi.co/rev/#{rev_id}", :green]}"
    rescue Errno::ECONNRESET, Errno::EINVAL, EOFError, Net::HTTPBadResponse,
           Net::HTTPHeaderSyntaxError, Net::OpenTimeout, Net::ProtocolError,
           Revit::BaemptyException, Revit::GraphQLDataException,
           Revit::GraphQLValidationException => e

      Revit::LOGGER.error(e.message)
      spinner.error(e.message)

      successful_docs.each do |id|
        RevAPI.delete_doc(id)
      end

      RevAPI.delete_rev(rev_id)
    end

    desc 'login', 'Get an auth token for accessing the rev api'
    long_desc <<-LONGDESC
      Sends a request for a valid auth token (jwt) to use for accessing the API.
      Refresh is handled automatically as along as the user keeps on using rev otherwise it'll expire after about a week.

      `revit login`
    LONGDESC
    def login
      puts "Login here for your token:\n\n#{Paint[Auth.auth_code_url, :green]}"
      puts "\n\n"

      auth_code = Thor::Shell::Basic.new.ask('Enter your auth code => ')
      spinner = TTY::Spinner.new('[:spinner] :msg', format: :bouncing_ball)

      spinner.update(msg: 'Logging in')
      spinner.run do
        begin
          auth_token = Auth.auth_token(auth_code)
          user_id = RevAPI.signin_user(auth_token[:auth0_id])

          if user_id.nil?
            spinner.update(msg: 'User not found! Creating...')
            user_id = RevAPI.create_user(auth_token[:auth0_id])
            spinner.success(Paint['User created and Logged in', :green])
          end

          Revit::STORE.transaction do |store|
            store[:access_token] = auth_token[:access_token].strip
            store[:expires] = auth_token[:expires]
            store[:auth0_id] = auth_token[:auth0_id].strip
            store[:user_id] = user_id
          end
          spinner.success(Paint['Login successful :)', :green])
        rescue Errno::ECONNRESET, Errno::EINVAL, EOFError,
               Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError,
               Net::OpenTimeout, Net::ProtocolError,
               Revit::GraphQLDataException,
               Revit::GraphQLValidationException => e

          Revit::LOGGER.error(e.message)
          spinner.error(Paint[e.message, :red])
        end
      end
    end
  end
end

# rubocop: enable Metrics/AbcSize, Metrics/MethodLength
