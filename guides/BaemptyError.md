# baemptyError

This simply means you have passed rev either to an empty repo or a [bare repo](http://www.saintsjd.com/2011/01/what-is-a-bare-git-repository/).

If you still want to use the repo:


- create a commit
``` bash

git add -A && git commit -m # Add every file in the repo

git add ./path/to/file.ext && git commit -m # Add a specific file or folder

```

- Destroy the repo
``` bash

rm -rf .git

```


Then try revit again:

``` bash

revit create ./path/to/repo

```
