# rubocop: disable Metrics/AbcSize, Metrics/ModuleLength

require 'rugged'
require 'tempfile'
require 'paint'

module Revit
  # A collection of utility functions for use with Revit
  #
  # @author [brwnrclse]
  module Util
    RevitDoc = Struct.new(:blob, :syntax, :doc_name, :has_diff)
    REVITSYNTAX = {
      '.rb' => 'RUBY',
      '.gemspec' => 'RUBY',
      '.py' => 'PYTHON',
      '.pyw' => 'PYTHON',
      '.pyd' => 'PYTHON',
      '.py3' => 'PYTHON',
      '.c' => 'C',
      '.cpp' => 'C',
      '.h' => 'C',
      '.hpp' => 'C',
      '.js' => 'JAVASCRIPT',
      '.jsx' => 'JAVASCRIPT',
      '.java' => 'JAVA',
      '.class' => 'JAVA',
      '.go' => 'GO',
      '.m' => 'OBJC',
      '.swift' => 'SWIFT',
      '.ps1' => 'POWERSHELL',
      '.lisp' => 'LISP',
      '.lsp' => 'LISP',
      '.json' => 'JSON',
      '.sbt' => 'SCALA',
      '.scala' => 'SCALA',
      '.yml' => 'YAML',
      '.yaml' => 'YAML',
      '.xml' => 'XML',
      '.sh' => 'SHELL',
      '.zsh' => 'SHELL',
      '.bash' => 'SHELL',
      '.md' => 'MARKDOWN',
      '.markdown' => 'MARKDOWN',
      '.fountain' => 'FOUNTAIN',
      '.diff' => 'DIFF'
    }.freeze

    #
    # Grabs all possible docs from the given paths
    #
    # @param      paths    The paths
    # @param      spinner  The spinner
    #
    # @return     [Array]
    #
    def self.collect_docs(paths, spinner)
      docs = []

      paths.each do |x|
        type = File.exist?(x) ? File.ftype(x) : 'other'

        if type == 'file' && File.zero?(x)
          spinner.update(msg: "#{Paint['Adding', nil, '#e3b505']} #{x}")
          spinner.run do
            docs.push(doc_from_path(x))
            spinner.success(Paint['document created', :green])
          end
        elsif type == 'directory'
          spinner.update(msg: "#{Paint['Adding', nil, '#e3b505']} docs from " \
                         "#{Paint[path, '#2de1fc']}")

          if Dir.entries(x).include?('.git') && !options[:no_vc]
            spinner.run do
              docs ||= docs_from_repo(x, spinner)
              spinner.success(Paint['repo docs created', :green])
            end
          else
            spinner.run do
              docs ||= docs_from_dir(x)
              spinner.success(Paint['dir docs created', :green])
            end
          end
        else
          spinner.update(msg: "#{Paint['Unsupported file:']} #{path}")
        end
      end

      docs
    end

    # Wrapper for creating a Document object from a file path
    #
    # @param path [String]
    #
    # @param has_diff [Boolean]
    #
    # @return [Revit::Util::RevDoc]
    #
    def self.doc_from_path(path, has_diff = false)
      ext = File.extname(path).downcase
      content_type = REVITSYNTAX[ext] ? REVITSYNTAX[ext] : 'PLAIN'

      RevitDoc.new(File.read(path).gsub("\r\n", "\n"), content_type,
                   File.basename(path), has_diff)
    end

    # Creates a list of documents from files in the passed directory
    #
    # @param path [String]
    #
    # @return [Array<Revit::Util::RevDoc>]
    #
    def self.docs_from_dir(path)
      docs = []
      paths = Dir.glob("#{path}/**/*").reject { |entry| Dir.exist?(entry) }

      paths.each do |entry|
        unless '..'.include?(entry) || File.zero?(entry)
          docs.push(doc_from_path(entry))
        end
      end

      docs
    end

    # Creates a list of documents from repo changelist
    #
    # @param path [String]
    #
    # @return [Array<Revit::Util:RevDoc>]
    #
    def self.docs_from_repo(path)
      docs = []
      repo = Rugged::Repository.discover(path)

      if repo.bare? || repo.empty?
        raise Revit::BaemptyException, "Bad repo: #{path}"
      end

      commit = repo.head.target
      diff = commit.parents.first.diff(commit)

      diff.find_similar!
      diff.each_delta do |d|
        file_path = d.new_file[:path]
        docs.push(doc_from_path("#{path}/#{file_path}", true))

        ofile = repo.lookup(d.old_file[:oid])
        nfile = repo.lookup(d.new_file[:oid])
        diff_file = Tempfile.new([File.basename(file_path).to_s, '.diff'])

        diff_file.write(ofile.diff(nfile).to_s)
        docs.push(doc_from_path(diff_file.path))
        diff_file.close
        diff_file.unlink
      end

      docs
    end
  end
end

# rubocop: enable Metrics/AbcSize, Metrics/ModuleLength
