require_relative 'helper_spec'

RSpec.describe Revit::Auth do
  it 'Can generate a url to visit for authentication' do
    expect(Sysrandom).to receive(:urlsafe_base64).and_return('')
    expect(Digest::SHA256).to receive_message_chain(:new, :update, :digest,
                                                    :to_i)

    Revit::Auth.auth_code_url
  end

  it 'Can exchange an auth code for an auth token' do
    fake_http = Net::HTTP.new('/')
    fake_response = {}

    expect(Net::HTTP).to receive(:new).and_return(fake_http)
    expect(fake_http).to receive(:request).and_return(fake_response)
    expect(fake_response).to receive(:body).and_return('{}')
    allow(Time).to receive(:now).and_return(0)

    Revit::Auth.auth_token('abc123')
  end
end
