require 'base64'
require 'digest'
require 'sysrandom'

module Revit
  # Convience module for handling our authentication actions and talking to
  #       Auth0
  #
  # @author [brwnrclse]
  #
  module Auth
    AUTH_CALLBACK_URL, AUTH_CLIENT_ID, AUTH0_ID, AUTH_STORE_ACCESS_TOKEN,
      AUTH_URL = Revit::STORE.transaction do |store|
        [
          store[:auth_callback_url],
          store[:auth_client_id],
          store[:auth0_id],
          store[:access_token],
          store[:auth_url]
        ]
      end
    VERIFIER = Sysrandom.urlsafe_base64(32)

    ##
    ## Helper Methods
    ##

    # Provides the user with a means to obtain an authorization code for
    #       accessing rev's api by opening a browser to our Auth0 login page
    #
    # @return [nil]
    #
    def self.auth_code_url
      verifier_challenge = Sysrandom.urlsafe_base64(
        Digest::SHA256.new.update(VERIFIER).digest.to_i
      )

      auth_code_url = AUTH_URL +
                      '/authorize?response_type=code&scope=openid%20profile' \
                      '&client_id=' + AUTH_CLIENT_ID +
                      '&redirect_uri=' + AUTH_CALLBACK_URL +
                      '&code_challenge=' + verifier_challenge +
                      '&code_challenge_method=S256'

      auth_code_url
    end

    # Exchanges the auth code obtained for a token used to access rev's api
    #
    # @param auth_code [String]
    #
    # @return [String]
    #
    def self.auth_token(auth_code)
      auth_token_uri = URI.parse('https://vaemoi.auth0.com/oauth/token')
      body = {
        grant_type: 'authorization_code',
        client_id: AUTH_CLIENT_ID,
        code_verifier: VERIFIER,
        code: auth_code,
        redirect_uri: AUTH_CALLBACK_URL
      }
      http = Net::HTTP.new(auth_token_uri.host, auth_token_uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      req = Net::HTTP::Post.new(auth_token_uri)
      req.content_type = 'application/json'
      req.body = body.to_json

      res = http.request(req)
      body = JSON.parse(res.body)
      token = {
        access_token: body['access_token'],
        auth0_id: body['id_token'],
        expires: Time.now.to_i + body['expires'].to_i
      }

      token
    end

    #
    # TODO: Add in signFile func for communicating with webtask
    #

    # Checks if the user has an Authentication token for accessing the API
    #
    # @return [Bool] true if token found
    #                raises an exception otherwise
    def self.logged_in
      if AUTH_STORE_ACCESS_TOKEN.nil? || AUTH_STORE_ACCESS_TOKEN.empty?
        raise Revit::LoginException
      end

      true
    end
  end
end
