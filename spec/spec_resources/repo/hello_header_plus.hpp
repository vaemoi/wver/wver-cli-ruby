#include <iostream>

class Node {
  public:
    Node();
    ~Node();

    int data;
    Node * next;
};
