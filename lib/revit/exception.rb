module Revit
  # Custom exception for dealing with repos in revit
  #
  # @author [bwrnrclse]
  #
  class BaemptyException < IOError
    def initialize(msg = '')
      super(msg)
    end
  end

  # Custom exception for dealing with graphql in revit
  #
  class GraphQLDataException < IOError
    def initialize(msg = '')
      super(msg)
    end
  end

  # Custom exception for dealing with graphql in revit
  #
  class GraphQLValidationException < IOError
    def initialize(msg = '')
      super(msg)
    end
  end

  # Custom exception for dealing with having the wrong graphql schema.
  #       User wille be prompted to run the --update_schema command
  #
  class GraphQLSchemaMismatchException < IOError
    def initialize(msg = '')
      super(msg)
    end
  end
end
