require 'graphlient'

module Revit
  # Intrface for rev API, connects local changes with our remote.
  #
  # @author [brwnrclse]
  #
  module RevAPI
    ENDPOINT_URL, ID_TOKEN, AUTH_TOKEN = Revit::STORE.transaction do |store|
      [store[:endpoint_url], store[:user_id], store[:auth0_id]]
    end

    CLIENT = Graphlient::Client.new(
      ENDPOINT_URL, headers: {
        'Authorization' => "Bearer #{AUTH_TOKEN}"
      }
    )

    ##
    ## Helper methods
    ##

    # Raise any errors in the response data or simply return the data
    #
    # @param api_response [Obj]
    #
    # @return [Array]
    #
    def self.handle_response(api_response)
      unless api_response.data
        error_info = []

        api_response.errors.each do |error|
          error_info.push("Error #{error&.code}: #{error&.message}")
        end

        raise Revit::GraphQLDataException, error_info.join(', ')
      end

      api_response.data
    end

    # Calls a query with the provided string and params
    #   and catches any validation or schema mismatch errors
    #
    def self.execute_query(querystr, params)
      if (params&.rev_id).length < 25
        raise Revit::GraphQLValidationException, 'Invalid rev id'
      end

      handle_response(CLIENT.query(querystr, params))
    end

    ##
    ## API methods
    ##

    # API call to createDocument
    # @param has_diff [Boolean]
    # @param doc_name [String]
    # @param rev_id [String]
    # @param syntax [String]
    #
    # @return [String]
    #
    def self.create_doc(doc_name, has_diff, rev_id, syntax)
      doc_name = 'Ole Noname Doc' if doc_name.nil? || doc_name.strip.empty?
      mutation = CLIENT.parse do
        mutation(syntax: :SYNTAX, name: :string, hasDiff: :boolean,
                 revId: :id) do
          createDocument(syntax: :syntax, name: :name, hasDiff: :hasDiff,
                         revId: :revId) do
            id
          end
        end
      end

      result = execute_query(mutation, syntax: syntax, rev: rev_id,
                                       name: doc_name, hasDiff: has_diff)

      result.createDocument.id
    end

    # API call to deleteDocument
    # @param id [String]
    #
    # @return [String]
    #
    def self.delete_doc(id)
      mutation = CLIENT.parse do
        mutation(id: :id) do
          deleteDocument(id: :id) do
            id
          end
        end
      end

      result = execute_query(mutation, id: id)

      result.deleteDocument.id
    end

    # API call to createRev
    # @param summary [String]
    # @param title [String]
    #
    # @return [String]
    #
    def self.create_rev(summary, title)
      summary = 'Some rev' if summary.nil? || summary.strip.empty?
      title = 'rev, right?' if title.nil? || title.strip.empty?
      mutation = CLIENT.parse do
        mutation(summary: :string, title: :string, userId: :id) do
          createRev(summary: :summary, title: :title, userId: :userId) do
            id
          end
        end
      end

      result = execute_query(mutation, summary: summary, title: title,
                                       userId: ID_TOKEN)

      result.createRev.id
    end

    # API call to deleteRev
    # @param id [String]
    #
    # @return [String]
    #
    def self.delete_rev(id)
      mutation = CLIENT.parse do
        mutation(id: :id) do
          deleteRev(id: :id) do
            id
          end
        end
      end

      result = execute_query(mutation, id: id)

      result.deleteRev.id
    end

    # Authenticate the user against the api to receive a user id
    # @param id [String]
    #
    # @return [String]
    #
    def self.create_user(auth0_id)
      mutation = CLIENT.parse do
        mutation(auth0SignUpData: AuthProviderSignupData!) do
          createUser(authProvider: auth0SignUpData) do
            id
          end
        end
      end

      result = execute_query(mutation, auth0_id: auth0_id)

      result.createUser.id
    end

    # Authenticate the user against the api to receive a user id
    # @param id [String]
    #
    # @return [String]
    #
    def self.signin_user(auth0_id)
      mutation = CLIENT.parse do
        mutation(auth0: :AUTH_PROVIDER_AUTH0) do
          signinUser(auth0: :auth0) do
            user do
              id
            end
          end
        end
      end

      result = execute_query(mutation, auth0_id: auth0_id)

      # This means a user is trying to sign in but doesn't have an account, yet
      result.nil? ? result : result.signinUser.user.id
    end
  end

  # TODO: Add in create_s3Resource func
end
