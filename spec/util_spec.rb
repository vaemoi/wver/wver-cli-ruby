require_relative 'helper_spec'

# rubocop:disable Metrics/BlockLength
RSpec.describe Revit::Util do
  it 'creates a document from a given file path' do
    path = "#{File.dirname(__FILE__)}/spec_resources/README.md"
    doc = Revit::Util::RevitDoc.new("# FILE THAT SAYS README\n", 'MARKDOWN',
                                    'README.md', false)

    result = Revit::Util.doc_from_path(path)

    expect(result).to eq(doc)
  end

  it 'creates a list of documents from a given directory' do
    path = "#{File.dirname(__FILE__)}/spec_resources/dir"

    expect(Revit::Util).to receive(:doc_from_path)
      .with("#{path}/hello_fountain.fountain").and_return({})

    expect(Revit::Util).to receive(:doc_from_path)
      .with("#{path}/hello_markdown.md").and_return({})

    expect(Revit::Util).to receive(:doc_from_path)
      .with("#{path}/hello_xml.xml").and_return({})

    expect(Revit::Util).to receive(:doc_from_path)
      .with("#{path}/hello_yaml.yaml").and_return({})

    docs = Revit::Util.docs_from_dir(path)

    expect(docs.length).to eq(4)
  end

  it 'creates docs for each file and diff in a commit' do
    create_repo

    repo_dir = "#{File.dirname(__FILE__)}/spec_resources/repo"
    repo = Rugged::Repository.new(repo_dir)

    File.open(File.join(repo_dir, 'README.md'), 'w') do |file|
      msg = "Written at #{Time.now.getutc}"
      file.write(msg)
    end

    oid = Rugged::Blob.from_workdir(repo, 'README.md')
    index = repo.index

    index.add(path: 'README.md', oid: oid, mode: 0o100644)

    options = {}
    options[:tree] = index.write_tree(repo)
    options[:author] = { email: 'foo@vaemoi.co', name: 'foo', time: Time.now }
    options[:committer] = { email: 'fo@vaemoi.co', name: 'fo', time: Time.now }
    options[:message] = "Testing revit with rugged #{Time.now.getutc}"
    options[:parents] = [repo.head.target].compact
    options[:update_ref] = 'HEAD'

    index.write
    Rugged::Commit.create(repo, options)

    expect(Revit::Util).to receive(:doc_from_path)
      .with("#{repo_dir}/README.md", true)
    expect(Revit::Util).to receive(:doc_from_path)

    docs = Revit::Util.docs_from_repo(repo_dir)
    expect(docs.length).to eq(2)
    remove_repo
  end

  it 'raises an error when passed an empty repo' do
    repo_dir = "#{File.dirname(__FILE__)}/spec_resources/repo_2"
    Dir.mkdir(repo_dir) unless Dir.exist?(repo_dir)
    Rugged::Repository.init_at(repo_dir, :bare)

    expect { Revit::Util.docs_from_repo(repo_dir) }.to raise_exception(
      Revit::BaemptyException
    )

    FileUtils.rm_rf(repo_dir)
  end
end
# rubocop:enable Metrics/BlockLength
