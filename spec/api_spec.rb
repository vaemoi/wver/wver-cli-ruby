require_relative 'helper_spec'

# rubocop:disable Metrics/BlockLength
RSpec.describe Revit::RevAPI do
  it 'can retrieve the from the endpoint' do
    expect(Revit::RevAPI::CLIENT.schema).to be_a(GraphQL::Schema)
  end

  it 'can retrieve and raise an error from a bad response' do
    fake_response = {}
    fake_errors = []

    expect(fake_response).to receive(:data).and_return(nil)
    expect(fake_response).to receive(:errors).and_return(fake_errors)

    expect { Revit::RevAPI.handle_response(fake_response) }.to raise_exception(
      Revit::GraphQLDataException
    )
  end

  it 'can create and delete document from the api' do
    result = {}

    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(result)
    expect(result).to receive_message_chain(:createDocument, :id)
      .and_return(nil)
    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(result)
    expect(result).to receive_message_chain(:deleteDocument, :id)
      .and_return(nil)

    Revit::RevAPI.delete_doc(Revit::RevAPI.create_doc(nil, false, '_', 'PLAIN'))
  end

  it 'can create and delete rev from the api' do
    result = {}

    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(result)
    expect(result).to receive_message_chain(:createRev, :id).and_return(nil)
    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(result)
    expect(result).to receive_message_chain(:deleteRev, :id).and_return(nil)

    Revit::RevAPI.delete_rev(Revit::RevAPI.create_rev(nil, nil))
  end

  #  it 'can raise exceptions for bad id lengths' do
  #
  #  end

  it 'can create a user given an auth0 id' do
    result = {}

    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(result)
    expect(result).to receive_message_chain(:createUser, :id).and_return(nil)

    Revit::RevAPI.create_user(nil)
  end

  it 'can signin a user' do
    result = {}

    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(result)
    expect(result).to receive_message_chain(:nil?).and_return(false)
    expect(result).to receive_message_chain(:signinUser, :user, :id)
      .and_return(nil)

    Revit::RevAPI.signin_user('_')
  end

  it 'can return nil if user tries to signin with no account' do
    fake_data = {}

    expect(Revit::RevAPI::CLIENT).to receive(:parse).and_return({})
    expect(Revit::RevAPI).to receive(:execute_query).and_return(fake_data)
    expect(fake_data).to receive_message_chain(:nil?).and_return(true)

    Revit::RevAPI.signin_user(nil)
  end
end
# rubocop:enable Metrics/BlockLength
