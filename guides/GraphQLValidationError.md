# GraphQLValidationError

Hey, don't fret this just means that some variable passed to our api wasn't in the correct format! It's not your fault we swear!

Try running the same command again, and if it fails post an issue to our tracker on Bitbucket [here](https://bitbucket.org/vaemoi/revit/issues)
