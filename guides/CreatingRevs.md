# How to

## Getting Started
---
To get started with revit make sure you have the latest ruby installed or atleast version 2.3.3.

``` bash

ruby -v 

```

Now let's install revit!

``` bash

gem install revit

revit -v
revit --version

```



## Creating a revs
---
revit only allows creating new rev at the moment, modifying them will come soong!

Get started by running revit ```create```

``` bash

revit create ./path/to/docs # Path to a single file, directory or git repo

revit create -l ./path/file1 ./path/file2 # -l allows you to pass multiple files / docs / repos

revit create ./path/file1 -d "An example rev" # Add a description to your rev
 
revit create ./path/file1 -t "First Rev" # Add a title to your rev

```

These commands will also be shown by running:

``` bash

revit --help

```
