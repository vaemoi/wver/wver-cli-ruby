# rubocop disable Metrics/BlockLength
# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'revit/version'

Gem::Specification.new do |spec|
  spec.name          = 'revit'
  spec.version       = Revit::VERSION
  spec.authors       = ['']
  spec.email         = ['brwnrclse@gmail.com']
  spec.summary       = 'This gem has moved to wver https://rubygems.org/gems/wver'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.3.0'

  spec.add_dependency 'graphlient'
  spec.add_dependency 'paint'
  spec.add_dependency 'svn', '~> 0.2.0'
  spec.add_dependency 'sysrandom'
  spec.add_dependency 'thor'
  spec.add_dependency 'tty-spinner'

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'pry', '~> 0.10.4'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.5', '>= 3.5.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'webmock'
end
