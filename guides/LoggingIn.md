# Logging In
Start an authentication workflow to receive an access token (allows you to access our api)

Running login will produce a url you need to follow in order to get your auth code from Auth0. Paste this code back into the terminal and you'll be authenticated. (If you want more security info checkout our wiki!)

``` bash

revit login

"Login here for your token": https://vaemoi.auth0/login/stuff/to/login

"Enter your auth code here" => owfnoisjIAIO90w9JDOFIEF

Authentication successful :)
```

Now you're ready to make some revs!
