require 'fileutils'
require 'rugged'
require 'simplecov'

SimpleCov.start do
  add_filter '/vendor/'
  add_filter '/spec/'
  add_filter '/lib/cli.rb'
  add_filter '/lib/schema/'
end

require 'revit'

# Turns the repo folder in spec_resources into a repo by creating a .git folder
#
# @return [type] [description]
def create_repo
  FileUtils.rm_rf("#{File.dirname(__FILE__)}/spec_resources/repo/.git")
  FileUtils.cp_r("#{File.dirname(__FILE__)}/spec_resources/repo/git",
                 "#{File.dirname(__FILE__)}/spec_resources/repo/.git")
end

# Turns the repo folder in spec_resources into a dir by removing the .git folder
#
# @return [type] [description]
def remove_repo
  FileUtils.rm_rf("#{File.dirname(__FILE__)}/spec_resources/repo/.git")
end

# rubocop: disable Security/Eval

# This code was adapted from Ruby on Rails, available under MIT-LICENSE
# Copyright (c) 2004-2013 David Heinemeier Hansson
def capture(stream)
  begin
    stream = stream.to_s
    eval("$#{stream} = StringIO.new", binding, __FILE__, __LINE__)
    yield
    result = eval("$#{stream}", binding, __FILE__, __LINE__).string
  ensure
    eval("$#{stream} = #{stream.upcase}", binding, __FILE__, __LINE__)
  end

  result
end

# rubocop: enable Security/Eval
