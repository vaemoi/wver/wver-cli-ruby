[![Code Climate](https://codeclimate.com/repos/57d861ee01430a66ea000bed/badges/4d214cda256127c2391e/gpa.svg)](https://codeclimate.com/repos/57d861ee01430a66ea000bed/feed) [![Test Coverage](https://codeclimate.com/repos/57d861ee01430a66ea000bed/badges/4d214cda256127c2391e/coverage.svg)](https://codeclimate.com/repos/57d861ee01430a66ea000bed/coverage) [![Issue Count](https://codeclimate.com/repos/57d861ee01430a66ea000bed/badges/4d214cda256127c2391e/issue_count.svg)](https://codeclimate.com/repos/57d861ee01430a66ea000bed/feed)

# Rev
---
Upload your documents/directories/git commits to rev for a code review session!

Conduct your code reviews with [rev](https://rev.vaemoi.co)!

[Bitbucket](https://bitbucket.org/vaemoi/revit)
[Docker Whale used for testing](https://hub.docker.com/r/vaemoi/revit-whale/)
[Issues](https://bitbucket.org/vaemoi/revit/issues)

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
